#!/bin/sh

SYNTAX="syntax: $0 [start <id>|stop|tag <id> <tag-with>|untag <id> <untag-with> | upload-to-jira | summary | watch-summary | timew <timew-command>]"

# Check if docker is installed

if [ ! "command -v docker &> /dev/null" ]; then
    echo "This script requires docker to be installed."
    exit 1
fi

# Check if image of workdaymanagement already exists
if ! docker image inspect workdaymanagement:latest 1> /dev/null ; then
    docker build -t workdaymanagement:latest .
fi

if [ ! -f .jirauser ]; then
    echo "Apparently first run, asking for Jira username for future use."
    sh set-jira-username.sh
fi

if [ ! -f .jiraurl ]; then
    echo "Apparently first run, asking for Jira address for future use. For example, enter \"https://our.jira.com\""
    sh set-jira-address.sh
fi

BASE_COMMAND="docker run -it --rm  --name workdaymanagement -e JIRAUSER=`cat .jirauser` -e JIRAURL=`cat .jiraurl` -v timew_data:/root/timewarrior workdaymanagement:latest"

if [ "$1" = "start" ];
then
    shift
    $BASE_COMMAND timew start $1
elif [ "$1" = "stop" ];
then
    $BASE_COMMAND timew stop
elif [ "$1" = "upload-to-jira" ];
then
    $BASE_COMMAND /root/workday-ended-tasks
elif [ "$1" = "summary" ];
then
    $BASE_COMMAND timew summary :ids
elif [ "$1" = "watch-summary" ];
then
    $BASE_COMMAND watch -n 5 timew summary :ids
elif [ "$1" = "tag" ];
then
    shift
    ID_TO_TAG=$1
    shift
    TAG_WITH=$1
    $BASE_COMMAND timew tag $ID_TO_TAG $TAG_WITH
elif [ "$1" = "untag" ];
then
    shift
    ID_TO_UNTAG=$1
    shift
    UNTAG_WITH=$1
    $BASE_COMMAND timew untag $ID_TO_UNTAG $UNTAG_WITH
elif [ "$1" = "timew" ];
then
    shift
    TIMEW_COMMAND=$1
    $BASE_COMMAND timew $1
else
    echo $SYNTAX
    exit 1
fi
