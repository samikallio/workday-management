FROM debian:buster

ENV SDKMAN_DIR=/root/.sdkman

RUN apt-get update && \
    rm /bin/sh && ln -s /bin/bash /bin/sh && \
    apt-get install -y -qq timewarrior curl wget unzip zip gnupg \ 
    software-properties-common locales watch

ENV TIMEWARRIORDB=/root/timewarrior \
KOTLIN_HOME="$SDKMAN_DIR/candidates/kotlin/current" \
KSCRIPT_HOME="$SDKMAN_DIR/candidates/kscript/current" \
GRADLE_HOME="$SDKMAN_DIR/candidates/gradle/current" \
PATH="$KOTLIN_HOME/bin:$KSCRIPT_HOME/bin:/root:$GRADLE_HOME/bin:$PATH" \
LANG=fi_FI.UTF-8 \
LANGUAGE=fi:en \
TZ=Europe/Helsinki

RUN sed -i -e 's/# fi_FI.UTF-8 UTF-8/fi_FI.UTF-8 UTF-8/' /etc/locale.gen && \
    locale-gen

COPY Dataclasses.kt /root
COPY UnirestObjectMapper.kt /root
COPY workday-ended-tasks.kts /root
COPY set_up_container.sh /root

RUN chmod u+x /root/set_up_container.sh && /root/set_up_container.sh
