#!/bin/sh

wget -O- https://apt.corretto.aws/corretto.key | apt-key add - && \
    add-apt-repository 'deb https://apt.corretto.aws stable main' && \
    apt-get update && \
    apt-get install -y -qq java-1.8.0-amazon-corretto-jdk && \
    curl -s "https://get.sdkman.io" | bash

source "$HOME/.sdkman/bin/sdkman-init.sh" && \
    sdk install kscript && \
    sdk install kotlin && \
    sdk install gradle && \
    

kscript --package root/workday-ended-tasks.kts && mv /workday-ended-tasks /root/ && chmod a+x /root/workday-ended-tasks && rm -rf /root/.sdkman

apt-get remove -y -qq curl wget unzip \
    zip gnupg software-properties-common java-1.8.0-amazon-corretto-jdk && \
    apt-get autoremove -y

apt-get install -y default-jre-headless && apt-get clean