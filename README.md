# Script to upload your work to Jira and print entries as time used and Jira issue topic

The time is calculated as 15 minute intervals, so 0.25h for 15 minutes, 0.5h for half an hour.

Provided "as-is", this might not work for you. 

## Requirements
To use this script, you must have setup as following.

### You track your work with Jira issues

No extra tags, just Jira issue key, just as `ABC-1234` or `DEF-123` etc. The issue must be in Jira.

### Timewarrior

The script assumes you are using [Timewarrior](https://timewarrior.net/) to track your time.

For instance, you track your ABC-1234 issue like this
```
workday-management.sh start ABC-1234
```

and stop your day with 
```
workday-management.sh stop
```

Timewarrior allows worklog to exported as JSON, so the script uses that to post your work to Jira. 

### Kscript
The uploading to Jira is done with Kotlin script and it is executed with [Kscript](https://github.com/holgerbrandl/kscript). 

## Usage
The script first checks if the tags in your Timewarrior summary are found in Jira as issues. If issue is found, the script takes its summary and posts the time. Jira then subtracts this from the estimated time. This allows you to track how much time left you have for the issue. 

When your daily tasks are done, run the script with
```
workday-management.sh upload-to-jira 
```

In the first run, the script will ask your Jira username and the URL for the Jira you are using. It saves this information, and next time askes just your password.

When script is done, it prints out entries with time converted to hours, which you can add to whatever system you are using.

## Examples
Here is how I track my time. 

When I start my day, I run 
```
workday-management.sh watch-summary
```
This will print the summary for day's tasks in 5 second intervals.


and 
``` 
workday-management.sh start ABC-1234
```
where ABC-1234 is the key for the issue I'm working with.

And when my workday is complete, I run
```
workday-management.sh stop
```
and 
```
workday-management.sh upload-to-jira
```

The command prints the Jira issues, their keys and topic and the time used in hours.

Jira issue has now comment about worklog 

![alt text](example-worklog-entry.png)

Jira time tracking also has entry 

![alt text](example-jira-tracking.png)

This example issue had no estimated time, but if it had, Jira would had decreased it with logged work.

## TODO
* The generated Docker image is huge, slim it down
* More documentation
* More examples
