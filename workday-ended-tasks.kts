#!/usr/bin/env kscript

@file:MavenRepository("jcenter","https://jcenter.bintray.com/")

@file:DependsOn("com.mashape.unirest:unirest-java:1.4.9")
@file:DependsOn("org.json:json:20140107")
@file:DependsOn("com.fasterxml.jackson.core:jackson-databind:2.9.9")

@file:Include("UnirestObjectMapper.kt")
@file:Include("Dataclasses.kt")

import java.util.concurrent.TimeUnit
import java.io.InputStream
import java.io.IOException
import com.mashape.unirest.http.Unirest
import com.mashape.unirest.http.ObjectMapper
import com.mashape.unirest.http.JsonNode
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.CopyOnWriteArrayList

val unirestObjectMapper = UnirestObjectMapper()

val jiraAddress = System.getenv("JIRAURL")

println("This script will upload your worklog to Jira and print entries with Jira issue description")
println("Script requires timewarrior and a Jira account.")
val username = System.getenv("JIRAUSER")

if (jiraAddress == null || username == null) {
    println("Either .jiraddress or .jirauser was null, so cannot continue, bailing out. Please fix.")
    System.exit(-1)
}

val jiraRestApiAddress = "${jiraAddress}/rest/api/latest/"

val passwordConsole = System.console()
println("Enter your JIRA password: ")
val passwordChars = passwordConsole.readPassword()
val password = String(passwordChars)
val jiraLogEntryList = mutableListOf<JiraWorklogEntry>()
val timeTolerance = 10
val baseTimeStart = 0

val result = "timew export from today".runCommand()
result?.let {
    Unirest.setObjectMapper(unirestObjectMapper)

    val resultList = mutableListOf<String>()
    val scanner = Scanner(it)
    var stringvalue = scanner.next()

    while(scanner.hasNext()) {
        val value = scanner.next()
        if (value != "[" && value != "]") {
            resultList.add(value)
        }
    }

    val entryList = parseListToEntries(resultList)

    println("Posting worklogs to Jira")

    for(entry in entryList) {
        val jiraLogEntry = convertEntryToJiraIssueIfFound(entry)
        jiraLogEntry?.let {
            jiraLogEntryList.add(it)
            logJiraWorkForWorkElement(it, entry.tags.first())
        }
    }

    println("Printing ValueFrame entries")
    println()

    val valueFrameEntries = mutableMapOf<String, ValueFrameEntry>()

    jiraLogEntryList.forEach {
        val existingEntry = valueFrameEntries[it.issue]
        if (existingEntry == null) {
            val newEntry = ValueFrameEntry(issue = it.issue, summary = it.summary,
                    timeSpentSeconds = it.timeSpentSeconds)
            valueFrameEntries[it.issue] = newEntry
        } else {
            val updatedEntry = ValueFrameEntry(issue = existingEntry.issue, 
            summary = existingEntry.summary, 
            timeSpentSeconds = existingEntry.timeSpentSeconds + it.timeSpentSeconds)
            valueFrameEntries[it.issue] = updatedEntry
        }
    }



    for (value in valueFrameEntries.values) {
        println()
        println("${value.issue}: ${value.summary}")
        println(secondsToFractionalHours(value.timeSpentSeconds))
        println()
    }

    println("Done!")
}

fun secondsToFractionalHours(seconds: Long): String {
    var wholeHours = seconds / 60 / 60;
    val wholeMinutes = (seconds % 3600) / 60;
    var fractionalHours = "0"
    if (wholeMinutes > baseTimeStart && wholeMinutes <= (baseTimeStart + (timeTolerance * 2))) {
        fractionalHours = "25"
    }

    if (wholeMinutes > (baseTimeStart + (timeTolerance * 2)) 
            && wholeMinutes <= (baseTimeStart + (timeTolerance * 3))) {
        fractionalHours = "5"
    }

    if (wholeMinutes > (baseTimeStart + (timeTolerance * 3)) 
            && wholeMinutes <= (baseTimeStart + ( timeTolerance * 4))) {
        fractionalHours = "75"
    }

    if (wholeMinutes > (baseTimeStart + (timeTolerance * 4))) {
        wholeHours++
    }

    return "$wholeHours.$fractionalHours"
}

fun parseListToEntries(rawList: List<String>): List<Entry> {
    val resultList = mutableListOf<Entry>()
    for(rawValue in rawList) {
        resultList.add(unirestObjectMapper.readValue(rawValue, Entry::class.java))
    }
    return resultList
}

fun String.runCommand(): String? {
    try {
        val parts = this.split("\\s".toRegex())
        val proc = ProcessBuilder(*parts.toTypedArray())
            .redirectOutput(ProcessBuilder.Redirect.PIPE)
            .redirectError(ProcessBuilder.Redirect.PIPE)
            .start()

        proc.waitFor(30, TimeUnit.SECONDS)
        return proc.inputStream.bufferedReader().readText()
    } catch(e: IOException) {
        e.printStackTrace()
        return null
    }
}

fun entryLengthInSeconds(entry: Entry): Long {
    entry.end?.let {
        return (it.time - entry.start.time) / 1000
    }
    return 0
}

fun jiraWorkLogToJiraRestWorkLog(jiraWorklogEntry: JiraWorklogEntry): JiraRestWorklogEntry =
        JiraRestWorklogEntry(timeSpentSeconds = jiraWorklogEntry.timeSpentSeconds, comment = jiraWorklogEntry.comment)

fun convertEntryToJiraIssueIfFound(entry: Entry): JiraWorklogEntry? {
    findJiraIssue(entry.tags.first())?.let {
        return JiraWorklogEntry(issue = it.issue, summary = it.summary, comment = entry.comment,
            timeSpentSeconds = entryLengthInSeconds(entry))
    }

    return null
}

fun findJiraIssue(issue: String): JiraIssue? {
    val postAddress = "${jiraRestApiAddress}/${issue}/"
    val jsonResponse = Unirest.get(postAddress)
        .basicAuth("${username}", "${password}")
        .header("accept", "application/json")
        .queryString("fields", "summary")
        .asJson();
    if (jsonResponse.getStatus() == 200) {
        val mappedValue = unirestObjectMapper.readValue(jsonResponse.getBody().toString(), JiraResult::class.java)
        return JiraIssue(issue = issue, summary = mappedValue.fields.summary)
    } else {
        return null;
    }
}

fun logJiraWorkForWorkElement(jiraWorklogEntry: JiraWorklogEntry, issue: String) {
    val postAddress = "${jiraRestApiAddress}/${issue}/worklog"
    val jsonResponse = Unirest.post(postAddress)
        .basicAuth("${username}", "${password}")
        .header("accept", "application/json")
        .header("Content-Type", "application/json")
        .body(jiraWorkLogToJiraRestWorkLog(jiraWorklogEntry))
      .asString();
}