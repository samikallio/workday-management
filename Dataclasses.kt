class JiraResult() {
    lateinit var expand: String
    lateinit var self: String
    lateinit var id: String
    lateinit var key: String
    lateinit var fields: Field
}

class Field() {
    lateinit var summary: String
}

class Entry() {
    var comment = "Työajankirjaus"
    lateinit var start: Date
    var end: Date? = null
    var tags = mutableListOf<String>()
}

data class JiraIssue(val issue: String, val summary: String)
data class JiraWorklogEntry(val issue: String,
                            val summary: String,
                            val comment: String,
                            val timeSpentSeconds: Long)
data class JiraRestWorklogEntry(val timeSpentSeconds: Long, val comment: String)
data class ValueFrameEntry(val issue: String, val summary: String, val timeSpentSeconds: Long)