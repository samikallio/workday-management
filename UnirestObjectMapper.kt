class UnirestObjectMapper(): ObjectMapper {

    val dateParser: SimpleDateFormat

    init {
        dateParser = SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'")
        dateParser.timeZone = SimpleTimeZone(0, "GMT")
    }

    val jacksonObjectMapper = com.fasterxml.jackson.databind.ObjectMapper()
        .setDateFormat(dateParser)

    override fun <T> readValue(value: String, valueType: Class<T>): T {
        try {
            return jacksonObjectMapper.readValue(value, valueType)
        } catch (e: IOException) {
            throw RuntimeException(e)
        }
    }

    override fun writeValue(value: Any): String {
        try {
            return jacksonObjectMapper.writeValueAsString(value)
        } catch (e: JsonProcessingException) {
            throw RuntimeException(e)
        }
    }
}